using Technical.Scripts.Player;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private PlayerConfiguration configuration;
    public PlayerConfiguration GetConfig() => configuration;
    private Rigidbody m_rigidBody;

    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody>();
    }

    public string GetName()
    {
        return configuration.name;
    }

    public void Die()
    {
        Debug.LogError("PLAYER DEATH NOT IMPLEMENTED");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}