using Technical.Scripts.Enemy;
using UnityEngine;
using UnityEngine.Events;

namespace Technical.Scripts.Player
{
    [RequireComponent(typeof(PlayerController))]
    public class PlayerHealthController : MonoBehaviour
    {
        private PlayerController m_pc;
        private float pc_health;
        private float m_health;

        private float Health
        {
            get => m_health;
            set
            {
                if (value < 0) // damage
                    SetInvulnerable();
                m_health += value;
                OnHealthUpdate.Invoke(m_health, m_health / pc_health);
                if (m_health > 0) return;
                // death
                OnDie.Invoke();
                m_pc.Die();
            }
        }

        private bool bInvulnerable = false;
        
        public UnityEvent<float, float> OnHealthUpdate = new UnityEvent<float, float>();
        public UnityEvent OnDie = new UnityEvent();
        
        private void Awake()
        {
            m_pc = GetComponent<PlayerController>();
            m_health = m_pc.GetConfig().health;
            pc_health = m_health; // caching
        }

        private void Start()
        {
            OnHealthUpdate.Invoke(m_health, m_health / pc_health);
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("DamagePoint"))
            {
                Hit(other.GetComponent<DamagePointController>().GetDamage());
            }
        }

        private void Hit(float Damage)
        {
            print(bInvulnerable);
            if (bInvulnerable) return;
            Health = -Damage;
        }

        private void SetInvulnerable()
        {
            bInvulnerable = true;
            Invoke(nameof(ResetInvulnerable), 1f); // self toggle off
        }
        
        private void ResetInvulnerable()
        {
            bInvulnerable = false;
        }
    }
}
