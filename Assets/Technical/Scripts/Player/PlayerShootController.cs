using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class PlayerShootController : MonoBehaviour
{
    [SerializeField] private Transform[] shootPoints;
    [SerializeField] private int maxAttacks = 3;
    private int m_maxAttacks = 3;
    [SerializeField] private GameObject bullet;
    
    public UnityEvent<int, float> OnMaxAttacksUpdate = new UnityEvent<int, float>();
    
    private float m_lastFireAxis = 0f;
    private bool bShooting = false;

    private void Start()
    {
        OnMaxAttacksUpdate.Invoke(maxAttacks, 2f);
        m_maxAttacks = maxAttacks;
    }

    void Update()
    {
        var fAxis = Input.GetAxisRaw("Fire1");
        if (fAxis == m_lastFireAxis) return;
        m_lastFireAxis = fAxis;
        if (m_lastFireAxis == 0f) return;
        Shoot();
    }

    private void Shoot()
    {
        if (bShooting || m_maxAttacks == 0)
        {
            return;
        }
        
        
        if (m_maxAttacks == maxAttacks)
        {
            Invoke(nameof(StackMaxAttack), 2f);
        }
        
        m_maxAttacks--;
        OnMaxAttacksUpdate.Invoke(m_maxAttacks, 2f);

        StartCoroutine(ShootBullets(MouseManager.GetMousePositionInWorld()));
    }

    private IEnumerator ShootBullets(Vector3 worldPosition)
    {
        bShooting = true;
        
        for (var i = 0; i < 4; ++i)
        {
            var shootPoint = shootPoints[i % 2 == 0 ? 0 : 1];
            var go = Instantiate(bullet, shootPoint.position, shootPoint.rotation);
            go.layer = gameObject.layer;
            go.transform.LookAt(worldPosition);
            yield return new WaitForSeconds(0.1f);
        }

        bShooting = false;
    }

    private void StackMaxAttack()
    {
        if (m_maxAttacks >= maxAttacks)
        {
            return;
        }
        m_maxAttacks++;
        OnMaxAttacksUpdate.Invoke(m_maxAttacks, 2f);
        Invoke(nameof(StackMaxAttack), 2f);
    }
}
