using Technical.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.Player
{
    public class PlayerUIController : MonoBehaviour
    {
        [SerializeField] private Text playerName;
        [SerializeField] private HealthBarController Healthbar;
        [SerializeField] private AttackBarSlotController[] attackSlots;

        private PlayerController m_pc;
        private PlayerShootController m_psc;

        private void Awake()
        {
            m_pc = transform.parent.GetComponent<PlayerController>();
            m_psc = transform.parent.GetComponent<PlayerShootController>();
            m_psc.OnMaxAttacksUpdate.AddListener(AfterMaxAttackUpdate);
            
            playerName.text = m_pc.GetName();
            Healthbar.AfterHealthUpdate(m_pc.GetConfig().health, 1);
        }

        private void AfterMaxAttackUpdate(int currentMaxAttacks, float cooldown)
        {
            for (var i = 0; i < attackSlots.Length; ++i)
            {
                attackSlots[i].SetActive(currentMaxAttacks > i);
                attackSlots[i].SetOnCooldown(currentMaxAttacks == i, cooldown,
                    i < attackSlots.Length - 1 ? attackSlots[i + 1].GetElapsed() : 0);
            }
        }

        public void AfterHealthUpdate(float Amount, float Rate)
        {
            Healthbar.AfterHealthUpdate(Amount, Rate);
        }
    }
}