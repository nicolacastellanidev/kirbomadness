﻿using UnityEngine;

namespace Technical.Scripts.Player
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "Player/Configuration", order = 1)]
    public class PlayerConfiguration : ScriptableObject
    {
        public string name;
        public float health;
        public float level = 1;
    }
}