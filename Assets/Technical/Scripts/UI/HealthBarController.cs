using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Technical.Scripts.UI
{
    public class HealthBarController : MonoBehaviour
    {
        [SerializeField] private Text healthLabel;
        [SerializeField] private Image healthFiller;
        [SerializeField] private Image healthFillerPrevious;
        [SerializeField] private Image healthBorders;
        [SerializeField] private Color borderColor = Color.black;
        [SerializeField] private Color borderHitColor = Color.red;
        private Coroutine previousFillerAlignCoroutine;
        
        public void AfterHealthUpdate(float amount, float rate)
        {
            healthLabel.text = amount.ToString();
            var localScale = healthFiller.rectTransform.localScale;
            localScale.x = rate;
            healthFiller.rectTransform.localScale = localScale;
            if (rate == 1)
            {
                healthBorders.color = borderColor;
                return;
            }
            healthBorders.color = borderHitColor;
            if (previousFillerAlignCoroutine != null) StopCoroutine(previousFillerAlignCoroutine);
            previousFillerAlignCoroutine = StartCoroutine(AlignPrevious());
        }

        private IEnumerator AlignPrevious()
        {
            yield return new WaitForSeconds(0.5f);
            var localScale = healthFillerPrevious.rectTransform.localScale;
            var targetLocalScale = healthFiller.rectTransform.localScale;

            while (Math.Abs(localScale.x - targetLocalScale.x) > 0.01f)
            {
                healthBorders.color = Color.Lerp(healthBorders.color, borderColor, .15f);
                localScale.x = Mathf.Lerp(localScale.x, targetLocalScale.x, .1f);
                healthFillerPrevious.rectTransform.localScale = localScale;
                yield return new WaitForEndOfFrame();
            }
            healthBorders.color = borderColor;
            healthFillerPrevious.rectTransform.localScale = targetLocalScale;
        }

    }
}
