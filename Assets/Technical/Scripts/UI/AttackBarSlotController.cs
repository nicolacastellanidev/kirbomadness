using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AttackBarSlotController : MonoBehaviour
{
    [SerializeField] private Image filler;
    private bool bInCooldown = false;
    private Coroutine cooldownCoroutine;
    private float elapsed = 0f;
    public void SetActive(bool bActive)
    {
        SetFillerLocalScale(bActive ? 1 : 0);
        SetFillerColor(bActive ? Color.white : Color.black);
    }
    public void SetOnCooldown(bool bCooldown, float amount, float previousElapsed)
    {
        if (bInCooldown && !bCooldown)
        {
            StopCoroutine(cooldownCoroutine);
            elapsed = 0;
            return;
        }

        if (!bCooldown) return;
        elapsed = previousElapsed;
        cooldownCoroutine = StartCoroutine(Cooldown(amount));
    }

    public float GetElapsed()
    {
        return elapsed;
    }

    private IEnumerator Cooldown(float amount)
    {
        bInCooldown = true; // lock
        while (Math.Abs(elapsed - amount) > 0.01f)
        {
            SetFillerLocalScale(elapsed/amount);
            SetFillerColor(Color.Lerp(Color.gray, Color.white, elapsed/amount));
            elapsed += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        SetFillerLocalScale(1f);
        elapsed = 0;
        bInCooldown = false; // unlock
    }

    private void SetFillerColor(Color color)
    {
        filler.color = color;
    }
    private void SetFillerLocalScale(float scaleX)
    {
        var localScale = filler.rectTransform.localScale;
        localScale.x = scaleX;
        filler.rectTransform.localScale = localScale;
    }
}
