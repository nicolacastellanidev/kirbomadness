using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class DamageUIController : MonoBehaviour
{
    [SerializeField] private Text amountLabel;
    private Vector3 targetMovement;

    private void Start()
    {
        targetMovement = transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(0, 1f), 0f);
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, targetMovement, .1f);
    }

    public void Setup(float amount)
    {
        amountLabel.text = amount.ToString();
    }

}
