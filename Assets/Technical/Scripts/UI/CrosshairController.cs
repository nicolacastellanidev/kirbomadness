using System;
using UnityEngine;
using UnityEngine.UI;

public class CrosshairController : MonoBehaviour
{
    [SerializeField] private Image crosshairImage;
    [SerializeField] private Sprite defaultSprite;
    [SerializeField] private Sprite canHitSprite;

    void LateUpdate()
    {
        transform.position = MouseManager.GetMousePositionInWorldWithObjects();
        var hoveredGameObject = MouseManager.GetHoveredGameObject();
        SetCanHitCrosshair(hoveredGameObject && hoveredGameObject.CompareTag("Enemy"));
    }

    private void SetCanHitCrosshair(bool canHit)
    {
        crosshairImage.sprite = canHit ? canHitSprite : defaultSprite;
    }
}
