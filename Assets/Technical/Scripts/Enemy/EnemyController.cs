using System;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace Technical.Scripts.Enemy
{
    [Serializable]
    public struct EnemyData
    {
        public string name;
        public float health;    
        public float speed; // 0 = static
        public float respawnAfter;
        public AudioClip[] hitSounds;
    }

    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyData data;
        [SerializeField] private DamageUIController damageUI;
        [SerializeField] private GameObject[] hitEffects;
        [SerializeField] private GameObject[] deathEffects;
        private float m_health;
        private bool bIsDead = false;

        private Animator m_animator;
        private AudioSource m_source;
    
        public UnityEvent<float, float> OnHealthUpdate = new UnityEvent<float, float>();
    
        public EnemyData GetData()
        {
            return data;
        }

        private void Awake()
        {
            m_source = GetComponent<AudioSource>();
            m_animator = GetComponent<Animator>();
        }

        private void Start()
        {
            m_health = data.health;
            OnHealthUpdate.Invoke(m_health, 1);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (bIsDead)
            {
                return;
            }
            if (!other.gameObject.CompareTag("Bullet")) return;
            if (other.gameObject.layer != gameObject.layer)
            {
                // opposite projectile
                TakeDamage(other.gameObject.GetComponent<BulletController>().GetData().damage, other.contacts[0].point);
            }
        }

        private void TakeDamage(float amount, Vector3 point)
        {
            var position = transform.position;
            var rotation = transform.rotation;
            m_health -= amount;
            OnHealthUpdate.Invoke(m_health, m_health/data.health);
        
            m_animator.Play("Hit");
        
            if(!m_source.isPlaying)
                m_source.Stop();
         
            m_source.PlayOneShot(data.hitSounds[Random.Range(0, data.hitSounds.Length)]);
        
            var go = Instantiate(damageUI, point, Quaternion.identity);
            go.Setup( amount );
        
            if(hitEffects.Length > 0)
                Instantiate(hitEffects[Random.Range(0, hitEffects.Length)], position, rotation);
        
            if (m_health > 0f) return;
            if (data.respawnAfter > 0)
            {
                ToggleEnemy(false);
                bIsDead = true;
                Invoke(nameof(Respawn), data.respawnAfter);
            }
            else
            {
                if(deathEffects.Length > 0)
                    Instantiate(deathEffects[Random.Range(0, deathEffects.Length)], position, rotation);
                Destroy(gameObject);
            }
        }

        private void Respawn()
        {
            ToggleEnemy(true);
            m_health = data.health;
            bIsDead = false;
            OnHealthUpdate.Invoke(m_health, m_health/data.health);
        }

        private void ToggleEnemy(bool bActive)
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(bActive);
            }
            GetComponent<CapsuleCollider>().enabled = bActive;
        }
    }
}