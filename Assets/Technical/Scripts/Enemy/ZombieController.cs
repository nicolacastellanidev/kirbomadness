using UnityEngine;
using UnityEngine.AI;

namespace Technical.Scripts.Enemy
{
    [RequireComponent(typeof(NavMeshAgent), typeof(EnemyController))]
    public class ZombieController : MonoBehaviour
    {
        [SerializeField, Header("Zombie configuration")]
        private float attackRange;
    
        [SerializeField] private Animator m_animator;
        private NavMeshAgent m_agent;
        private GameObject m_player;
        private EnemyController m_ec;
    
        private bool bAttacking = false;
        private static readonly int BMoving = Animator.StringToHash("bMoving");
        private static readonly int BAttacking = Animator.StringToHash("bAttacking");

        private void Awake()
        {
            m_agent = GetComponent<NavMeshAgent>();
            m_ec = GetComponent<EnemyController>();
            m_agent.speed = Random.Range(m_ec.GetData().speed, m_ec.GetData().speed * 2);
            m_animator.speed = m_agent.speed / m_ec.GetData().speed;
            m_player = GameObject.FindWithTag("Player");
        }
        
        private void Update()
        {
            m_animator.SetBool(BMoving, m_agent.velocity != Vector3.zero);
            var position = m_player.transform.position;
            transform.LookAt(position);
            bAttacking = Vector3.Distance(transform.position, position) <= attackRange;
            m_animator.SetBool(BAttacking, bAttacking);
            if(!bAttacking) 
                m_agent.destination = position;
            else
                m_animator.speed = 1.0f;
        }
    }
}
