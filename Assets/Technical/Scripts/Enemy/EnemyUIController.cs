using Technical.Scripts.UI;
using UnityEngine;
using UnityEngine.UI;

namespace Technical.Scripts.Enemy
{
    public class EnemyUIController : MonoBehaviour
    {
        [SerializeField] private Text name;
        [SerializeField] private HealthBarController Healthbar;
        private EnemyController m_ec;

        private void Awake()
        {
            m_ec = transform.parent.GetComponent<EnemyController>();
            m_ec.OnHealthUpdate.AddListener(AfterHealthUpdate);
        }

        private void AfterHealthUpdate(float Amount, float Ratio)
        {
            Healthbar.AfterHealthUpdate(Amount, Ratio);
        }

        private void Start()
        {
            var data = m_ec.GetData();
            name.text = data.name;
            Healthbar.AfterHealthUpdate(data.health, 1);
        }
    }
}