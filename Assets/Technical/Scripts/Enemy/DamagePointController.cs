using UnityEngine;

namespace Technical.Scripts.Enemy
{
    public class DamagePointController : MonoBehaviour
    {
        [SerializeField] private float Damage;
        public float GetDamage() => Damage;
    }
}
