using System;
using UnityEngine;

public class MouseManager : MonoBehaviour
{
    private static MouseManager instance;
    private Vector3 mousePositionInWorld = Vector3.zero;
    private Vector3 mousePositionInWorldWithObjects = Vector3.zero;
    private GameObject hoveredGameObject;
    private Camera m_camera;

    private void Awake()
    {
        if (instance && instance != this)
        {
            Destroy(this);
            return;
        }

        instance = this;
        m_camera = Camera.main;
        Cursor.visible = false;
    }

    private void LateUpdate()
    {
        var plane = new Plane(Vector3.up, 0);
        var ray = instance.m_camera.ScreenPointToRay(Input.mousePosition);

        if (plane.Raycast(ray, out var distance))
        {
            instance.mousePositionInWorld = ray.GetPoint(distance);
        }

        var hit = Physics.Raycast(ray.origin, ray.direction, out var hitInfo);
        if (hit)
        {
            instance.hoveredGameObject = hitInfo.transform.gameObject;
            instance.mousePositionInWorldWithObjects = hitInfo.point;
        }
        else
        {
            instance.hoveredGameObject = null;
            instance.mousePositionInWorldWithObjects = mousePositionInWorld;
        }
    }

    public static Vector3 GetMousePositionInWorld()
    {
        return instance.mousePositionInWorld;
    }

    public static Vector3 GetMousePositionInWorldWithObjects()
    {
        return instance.mousePositionInWorldWithObjects;
    }

    public static GameObject GetHoveredGameObject()
    {
        return instance.hoveredGameObject;
    }

}
