using System;
using UnityEngine;

[Serializable]
public class BulletData
{
    public float speed;
    public float damage;
}
public class BulletController : MonoBehaviour
{
    [SerializeField] private BulletData data;
    [SerializeField] private GameObject effectOnDestroy;

    public BulletData GetData()
    {
        return data;
    }
    
    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * Time.fixedDeltaTime * data.speed, ForceMode.Impulse);
    }

    private void OnCollisionEnter(Collision other)
    {
        Instantiate(effectOnDestroy, transform.position, transform.rotation);
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        GetComponent<SphereCollider>().enabled = false;
    }
}
