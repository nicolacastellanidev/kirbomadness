using UnityEngine;
using UnityEngine.Events;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField] private float lifetime = 1f;
    private void Start()
    {
        Invoke(nameof(DestroyMe), lifetime);
    }

    private void DestroyMe()
    {
        Destroy(gameObject);
    }
}