using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    private Camera m_camera;

    private void Start()
    {
        m_camera = Camera.main;
    }

    void LateUpdate()
    {
        var m_camTransform = m_camera.transform;
        transform.LookAt(transform.position + m_camTransform.rotation * Vector3.forward,
            m_camTransform.rotation * Vector3.up);
    }
}
